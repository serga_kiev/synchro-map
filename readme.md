# Syncro Map POC

## !!! Eosda has limited amoutn of photos/day to just 1, so current approach will not work

Apache configuration
`httpd.conf`:
- Enable modules:
```
LoadModule negotiation_module modules/mod_negotiation.so
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_http_module modules/mod_proxy_http.so
LoadModule proxy_html_module modules/mod_proxy_html.so
```
- Add proxy config:
```
<Location "/synchro-external/">
	ProxyPass "http://lms.eosda.com:80/"
	LogLevel proxy:debug
</Location>
```