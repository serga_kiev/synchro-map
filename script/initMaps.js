function initMap() {
  var myLatLng = {lat: 50.9738608, lng: 32.7546945};

  var map = new google.maps.Map(document.getElementById('google-map'), {
    zoom: 10,
    center: myLatLng
  });

  var infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);

  service.getDetails({
    placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
  }, function (place, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
      });
      google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
          'Place ID: ' + place.place_id + '<br>' +
          place.formatted_address + '</div>');
        infowindow.open(map, this);
      });
    }
  });

  var recenterSchub = function (lat, lng) {
    $("#shub-map").attr('src', 'http://www.etomesto.ru/map.php?map=2995&y=' + lat + '&x=' + lng);
  };

  var recenterLandsat = function (lat, lng) {
    const eosFrontHost = 'http://lv.eosda.com/';
    const localEosBackProxy = '/synchro-external/search/landsat8';
    //const eosBackHost = 'http://lms.eosda.com/';
    //const sceneId = 'LC81800252017062LGN00';

    const coordinatesObj = buildEosShapeObject(lat, lng);
    const searchQuery = 'sunElevation:[0+TO+90]+AND+cloudCoverFull:[0+TO+65]+AND+(acquisitionDate:[2017-02-01+TO+2017-03-31])+AND+dayOrNight:day';

    const eosQueryUrl = buildUrl(
      localEosBackProxy,
      {
        fields: 'sceneID,sunElevation,cloudCoverFull,acquisitionDate,data_geometry,browseURL,thumbnail,path,row,sensor,dayOrNight',
        limit: 3,
        search: encodeURI(searchQuery),
        shape: encodeURI(JSON.stringify(coordinatesObj)),
        sort_order: 'desc',
        sort_value: 'acquisitionDate'
      });

    // todo refetch only if marker is out of current image boundaries
    $.when(
      $.ajax({
        url: eosQueryUrl
      }))
      .done(function (data) {
        if (!data || _.isEmpty(data.results)) {
          throw Error("No matching Landsat images found");
        }
        var sceneId = data.results[0].sceneID;
        console.log('Landsat SceneId: ' + sceneId);
        console.log('Image Acquisition Date: ' + data.results[0].acquisitionDate);

        updateEosFrame(sceneId, lat, lng);
      })
      .fail(function () {
        console.error("Eos backend query failed");
      });

    var updateEosFrame = function (sceneId, lat, lng) {
      $("#landsat-map").attr('src', eosFrontHost
        + '?lat=' + lat + '&lng=' + lng
        + '&z=14&b=Red,Green,Blue,Panchrom&s=Landsat8&day=true&id='
        + sceneId
        + '&ir=2047,3124,2135,3099,2411,3346,0,0');
    };
  };

  // Listeners --------------------------------------------------------------------------------
  map.addListener(/*'center_changed'*/'mouseup', function () {
    var center = map.getCenter();

    $('#x-coord').text(center.lat());
    $('#y-coord').text(center.lng());

    recenterSchub(center.lat(), center.lng());
    recenterLandsat(center.lat(), center.lng());
  });

  map.addListener('click', function (e) {
    console.warn(e);
    //map.setZoom(8);
    map.setCenter(marker.getPosition());
  });

  // Utils --------------------------------------------------------------------------------
  var buildUrl = function (path, urlParams) {
    var fullUrl = path + '?';
    _.forIn(urlParams, function (value, key) {
      if (!_.isEmpty(value)) {
        fullUrl += key + '=' + value + '&';
      }
    });
    fullUrl = _.trimEnd(fullUrl, '&');
    return fullUrl;
  };

  var buildEosShapeObject = function (lat, lng) {
    var shift = 0.01;
    return {
      'type': 'Polygon',
      'coordinates': [
        [
          [lng + shift, lat + shift],
          [lng - shift, lat + shift],
          [lng - shift, lat - shift],
          [lng + shift, lat - shift],
          [lng + shift, lat + shift]
        ]
      ]
    };
  }
}